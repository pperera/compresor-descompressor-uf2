# Compresor-Descompressor UF2

## Enunciat:

### Es demana fer un programa en C que permeti simular la compressio i la descompressio d'un text.

## Estrategia seguida:

### 1. Demanem l'entrada d'un text per teclat.

### Una taula amb la relació entre caràcter, repetició i codi assignat.

### 3. Després compactem el text seguint els següents passos:

#### 3.1 Mirem quins caràcters hi ha en aquest text, i quants cops apareixen al text.

#### 3.2 Codifiquem els caràcters, més repetits, amb els codis binaris més curts i els menys repetits amb els codis més llargs.

#### 3.3 Codifiquem el text d'acord als codis trobats a l'apartat 2.2 .

### 4. Finalment demanem un altre cop el text per teclat, però, aquest cop el text compactat, i provem de descompactar-lo amb els mateixos codis trobats en la compactació, i així, obtenir el text original abans de compactar. Hem de treure per pantalla el resultat obtingut.

## Imatge amb el programa executat:
![Git](captura.png)

## El arxiu del codi del programa en C és el :
### [main.c](main.c)

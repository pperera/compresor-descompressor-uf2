/****************************************************
* Autor: Pau Nikita Perera Rocaspana.               *
* Data: 09/02/2022                                  *
* Versió: 1.0                                       *
* Projecte: Pràctica Compresor-Descompressor        *
* subprogrames: no n'hi ha.                         *
*                                                   *
*****************************************************/
//Afegir llibreries
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//Definir constants i/o macros
#define MAX_TEXT 100
#define MAX_REPETICIONS 100
#define MAX_CARACTERS 100
#define MAX_CODIS 43
#define MAX_DIGITS 5
#define BB while (getchar()!='\n')
//Declarac accions i funcions
void llista (char text[], char caracter[], int repeticions[],char codis[MAX_CODIS][MAX_DIGITS]);
void ordenar(char text[],char caracter[], int repeticions[], char codis[MAX_CODIS][MAX_DIGITS]);
void textcod(char text[],char text2[], char caracter[],  char codis[MAX_CODIS][MAX_DIGITS]);
void textdecod(char text3[],char text4[], char codis[][MAX_DIGITS], char caracter[], bool trobat, char buscacodis[]);
void oCodi(char text3[], char buscacodis[]);
bool iguals(char par1[], char par2[]);
void cercacod(char buscacodis, char codis[MAX_CODIS][MAX_DIGITS]);
void descompactar(char caracter[],bool trobat);

/*************************************************************************************************************************
* Programa principal "main()"                                                                                            *
* Descripció: Compta Fa una llista dels caracters no reptits (sobre unt text introduit per teclati les repetcions de     *
* caracter les afegeix a una segona columna de forma que et permeti ordenar la llista de més repeticions a menos         *
* i assignar al més repetit el codi més petit i el caracter menys reptit el codi més gran.                               *
*                                                                                                                        *
* Un cop feta la llista que vagi agafant el codi assignat al caracter i a mesura que vagi abanssant caracters del text   *
* introduït per teclat que vagi comparant el caracter amb el codi assignat i crea un text nou amb els codis separats     *
* per el simbolde '$' per formar el text codificat que s'imprimiria per pantalla.                                        *
*                                                                                                                        *
* Finalment introduiriem un text codificat revertiria el proces i surtirià impres de manera que estigues descodificat.   *
* Nota: la descodificació no s'imprimeix i per falta de temps (perque me topat en entrabancs al fer la practica) tampoc  *
*       se si funciona del tot be la descodificació al no poder veure la impressio del resultat.                         *
**************************************************************************************************************************/
int main()
{
    char text[MAX_TEXT+1];
    char text2[MAX_TEXT+1];
    char text3[MAX_TEXT+1];
    char text4[MAX_TEXT+1];
    char caracter[MAX_CARACTERS+1];
    char codis[MAX_CODIS][MAX_DIGITS]={"0","1","00","01","10","11","000","001","010","100","011","110","101","111","0000","0001","0010","0100","1000","0011","0110","1100","1001","0101","1010","0111","1110","1101","1011","1111","00000","00001","00010","00100","01000","10000","00011","00110","01100","11000","10001","01010","10100"};
    char buscacodis[MAX_TEXT+1];
    char par1;
    char par2;
    bool trobat;
    int repeticions[MAX_REPETICIONS];
    //Inicialitzacions
    int i=0;
    text[0]='\0';
    int it=0;
    while(it<=MAX_CARACTERS){
        caracter[it]='\0';
        it++;
    }
    it=0;
    while(it<MAX_REPETICIONS){
        repeticions[it]=0;
        it++;
    }
    //Programa principal
    printf("Introdueix text: ");
    scanf("%100[^\n]",text);
    BB;
    printf("\nCaracter \tRepeticions \tCodi");
    llista(text,caracter,repeticions,codis);
    ordenar(text,caracter,repeticions,codis);
    textcod(text,text2,caracter,codis);
    printf("\nel text compactat es: %s \n",text2);
    printf("Introdueix el text compactat: \n");
    scanf("%100[^\n]",text3);
    BB;
    iguals(par1, par2);
    textdecod(text3,text4,codis,caracter,trobat,buscacodis);
    printf("\nel text descompactat es: %s \n",text4);
    return 0;
}

/************************************************************************************************
* Nom: Fer llista dels caracters no repetits.                                                   *
* Descripció: A traves de un text crea una llista de tres columnes                              *
* -La 1r seria del caracter sense repeticions.                                                  *
* -La 2n Les vegades que es repetix el caracter.                                                *
* -La 3r El codi assignat.                                                                      *
* Paràmetres:                                                                                   *
* Ent/Sort: text[]. Vector de caràcters que representa                                          *
* el buffer on emmagatzemarà el text.                                                           *
* caracter: char, servira per afegir el caracters no repetits a la llista.                      *
* repeticions: enter, servira per afegir les vegades que es repterix cada vegada un caracter.   *
* codis: string, servira per afegir els codis a cada caracter.                                  *
* it: enter, servira per indicar la posicio en que estas del text.                              *
* ic: enter, servira per indicar la posicio on es tindra de ficar cada caracter.                *
************************************************************************************************/
void llista (char text[], char caracter[], int repeticions[], char codis[MAX_CODIS][MAX_DIGITS]){
    int it=0;
    while(text[it]!='\0'){
        int ic=0;
        while(text[it]!=caracter[ic] && caracter[ic]!='\0')ic++;
        if(caracter[ic]=='\0'){
            caracter[ic]=text[it];
        } else{repeticions[ic]++;
        }
        it++;
    }
}

/************************************************************************************************
* Nom: Ordenar els caracters de la llista segons les repeticions el més repetit a dalt de tot.  *
* Descripció: A traves la llista creada anteriorment ara la ordenarem                           *
*  de tal forma que el més repetit tingo el codi més petit de tal forma que aniran del codi més *
*  petit al més gran i de més repeitcions a menos.                                              *
*  Paràmetres:                                                                                  *
*  Ent/Sort: text[]. Vector de caràcters que representa el buffer on emmagatzemarà el text.     *
*  caracter: char[], servira per senyalitzar el caracter a imprimir.                            *
*  repeticions: int[], servira per senyalitzar les repeticions a imprimir d'un caracter.        *
*  codis: string[][] de 2 dimensions, servira per senyalitzar el codi a imprimir.               *
* ic: enter, servira per indicar el maxim de caracter en numero.                                *
* i: enter, servira per indicar la posicio del caracter de la llista.                           *
* j: enter, servira per indicar les vegades que es repterix cada caracter.                      *
* aux: enter, servira per a guardar dades numeriques temporalment (posicions).                  *
* auxchar: char, servira per a guardar caracter temporalment i poder-los moure de posició       *
************************************************************************************************/
void ordenar(char text[], char caracter[], int repeticions[],char codis[MAX_CODIS][MAX_DIGITS]){
    int ic=0;
    int i=0;
    int j=0;
    int aux=0;
    char auxchar;
    while(caracter[ic]!='\0') ic++;
    for(i=2;i<=ic;i++){
        for(j=0;j<=ic-i;j++){
            if(repeticions[j]<repeticions[j+1]) {
                aux=repeticions[j];
                repeticions[j]=repeticions[j+1];
                repeticions[j+1]=aux;
                auxchar=caracter[j];
                caracter[j]=caracter[j+1];
                caracter[j+1]=auxchar;
            }
        }
    }
    i=0;
    ic=0;
    while(caracter[ic]!='\0') ic++;
    while(i<ic){
     printf("\n%c \t\t%d \t\t%s", caracter[i],repeticions[i],codis[i]);
     i++;
    }
}

/************************************************************************************************
* Nom: Codificació                                                                              *
* Descripció: A traves la llista creada anteriorment ara compararem el codi del caracter amb    *
* el caracter del text introduit per teclat.                                                    *
*  Paràmetres:                                                                                  *
*  Ent/Sort: text[]. Vector de caràcters que representa el buffer on emmagatzemarà el text.     *
*  Ent/Sort: text2[]. Text que s'utilizara per a ecriure el nou text codificat.                 *
*  caracter: char[], servira per senyalitzar el caracter a imprimir.                            *
*  repeticions: int[], servira per senyalitzar les repeticions a imprimir d'un caracter.        *
*  codis: string[][] de 2 dimensions, servira per senyalitzar el codi a imprimir.               *
* ic: enter, servira per indicar el maxim de caracter en numero.                                *
* i: enter, servira per indicar la posicio del caracter de la llista.                           *
* it: enter, servira per indicar la posicio de a quin caracter estara del text                  *
* it2: enter, servira per indicar la posicio de a quin posicio estara del text2 i a on tindra   *
*      de escriure els caracter ja codificats                                                   *
************************************************************************************************/
void textcod(char text[],char text2[], char caracter[],  char codis[MAX_CODIS][MAX_DIGITS]){
    int it=0;
    int it2=0;
    while(text[it]!='\0'){
        int ic=0;
        while(text[it]!=caracter[ic] && caracter[ic]!='\0')ic++;
        int i=0;
        while(codis[ic][i]!='\0'){
            text2[it2]=codis[ic][i];
            i++;
            it2++;
        }
        text2[it2]='$';
        it2++;
        it++;
    }
    text2[it2]='\0';
}

/************************************************************************************************
* Nom: Codificació                                                                              *
* Descripció: A traves la llista creada anteriorment ara compararem el codi del caracter amb    *
* el caracter del text introduit per teclat.                                                    *
*  Paràmetres:                                                                                  *
*  Ent/Sort: text3[]. Vector de caràcters que representa el buffer on emmagatzemarà el text3.   *
*  Ent/Sort: text4[]. Text que s'utilizara per a ecriure el nou text codificat.                 *
*  caracter: char[], servira per senyalitzar el caracter a imprimir.                            *
*  buscacodis: char[], servira per busacr el codi a la llista.                                  *
*  trobat: boolear, servira per senyalitzar les repeticions a imprimir d'un caracter.           *
*  codis: string[][] de 2 dimensions, servira per senyalitzar el codi a convertira caracter.    *
* it3: enter, servira per indicar la posicio del caracter del text3                             *
* it4: enter, servira per indicar la posicio del caracter del text4 a escriure                  *
* subprogrames: hi ha 3 sub programes  i són els següents.                                      *
* oCodi: servira per obtenir el codi del text3                                                  *
* cercacod: servira per a buscar el codi i el caracter que s'ha obtingut al text3 a oCodi       *
* descompactar: servira per transformar cada codi del text codificat a el carcter normal i      *
*               formar el text4 (el descodificat)                                               *
************************************************************************************************/
void textdecod(char text3[],char text4[], char codis[MAX_CODIS][MAX_DIGITS], char caracter[],bool trobat, char buscacodis[]){
    int it3=0;
    int it4;
    while(text3[it3]!='\0'){
    //obtenir codi
    oCodi(text3,buscacodis);
    //cercar el codi "buscacodis" a la llista de paraules "codis"
    cercacod(buscacodis,codis);
    //descompactar
    descompactar(caracter,trobat);
    }
    text4[it4]='\0';
}



/************************************************************************************************
* Nom: oCodi                                                                                    *
* Descripció: el que fa es separar els codis per a poder treballar la conversió al caracter     *
*             corresponent                                                                      *
*  Paràmetres:                                                                                  *
*  Ent/Sort: text3[]. Vector de caràcters que representa el buffer on emmagatzemarà el text3.   *
*  buscacodis: char[], servira per busacr el codi a la llista.                                  *
* it3: enter, servira per indicar la posicio del caracter del text3                             *
* ic: enter, servira per indicar la posicio a on es copiara el caracter del text3 a buscacodis  *
************************************************************************************************/
void oCodi(char text3[], char buscacodis[]){
    int it3=0;
    int ic=0;
        while(text3[it3]!='$' && text3[it3]!='\0'){
            buscacodis[ic]=text3[it3];
            ic++;it3++;
        }
        buscacodis[ic]='\0';
        //saltar marca final de codi, el '$'
        if(text3[it3]=='$') it3++;
}
//cercar el codi "buscacodis" a la llista de paraules "codis"
/************************************************************************************************
* Nom:cercacod                                                                                  *
* Descripció: el que fa es separar els codis per a poder treballar la conversió al caracter     *
*             corresponent                                                                      *
*  Paràmetres:                                                                                  *
*  Ent/Sort: buscacodis: char[], servira per busacr el codi a la llista.                        *
*  Ent/Sort: codis[MAX_CODIS][MAX_DIGITS]. string de 2 dimencions que indicara el caracter de   *
*           la llista.                                                                          *
* bool: trobat, servira per indicar si es verdader o fals trobat.                               *
* ic: enter, servira per indicar la posicio al codi                                             *
************************************************************************************************/
void cercacod(char buscacodis, char codis[MAX_CODIS][MAX_DIGITS]){
    int ic=0; bool trobat=false;
        while(codis[ic][0]!='\0'){
            if(iguals(buscacodis,codis[ic])){
                trobat=true;break;
            }
        }
}

/************************************************************************************************
* Nom:descompactar                                                                              *
* Descripció: converteix el codi del text3 a caracter i el fica a text4                         *
*  Paràmetres:                                                                                  *
*  Ent/Sort: char: caracter[], servira per copiar el caracter a text4                           *
* bool: trobat, servira per indicar si es verdader o fals trobat i si es verdader fara el text4.*
* ic: enter, servira per indicar la posicio del caracter                                        *
* it4: enter, servira per indicar la posicio del caracter a escriure a text4.                   *
************************************************************************************************/
void descompactar(char caracter[],bool trobat){
    int ic=0;
    int it4=0;

    char text4[MAX_TEXT+1];
    if(trobat) {
            text4[it4]=caracter[ic];
            it4++;
        }
}

/************************************************************************************************
* Nom: iguals                                                                                   *
* Descripció: converteix el codi del text3 a caracter i el fica a text4                         *
*  Paràmetres:                                                                                  *
*  Ent/Sort: char:par1[], servira per coparar amb la par2.                                      *
*  Ent/Sort: char:par2[], servira per coparar amb la par1.                                      *
* i: enter, servira per indicar la posicio tant de par1 com de par2                             *
* return retorna par1[i]==par2[i]);                                                             *
************************************************************************************************/
bool iguals(char par1[], char par2[]){
    int i=0;
    while(par1[i]==par2[i] && (par1[i]!='\0' || par2[i]!='\0')) i++;
    return (par1[i]==par2[i]);
}

